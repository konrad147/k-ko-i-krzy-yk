#include "SerwerTCP.h"
#include "KlientTCP.h"

int main(int argc, char* argv[])
{
	if((argc > 1) && !strcmp(argv[1], "server"))
	{	
		SerwerTCP server;
		server.run();
	}else if((argc == 3) && !strcmp(argv[1], "client"))
	{
		KlientTCP klient(argv[2]);
		klient.run();
	}else
	{
		std::cout << "Niepoprawne opcje" << std::endl;
		std::cout << "Dla serwera: server" << std::endl;
		std::cout << "Dla klienta: client <ip>" << std::endl;
	}

}