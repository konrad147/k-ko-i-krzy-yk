#include <boost/asio.hpp>
#include "GraTCP.h"

#pragma once

class KlientTCP: public GraTCP
{
public:
	KlientTCP(const char* adres);
	virtual void run();

private:
	boost::asio::ip::tcp::resolver _Rozwiazywacz;
	boost::asio::ip::tcp::resolver::query _Zapytywacz;
	
	virtual void wyslijDane();
	virtual void odbierzDane();
};