#include "SerwerTCP.h"
#include <iostream>
#include <cstdlib>

using boost::asio::ip::tcp;
using namespace boost;

SerwerTCP::SerwerTCP(): _Akceptor(_Serwis, tcp::endpoint(tcp::v4(), 1500)), _zapelnione(0)
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			_Plansza[i][j] = PUSTE;
		}
	}
}

void SerwerTCP::run()
{
	std::system("clear");
	std::cout << "Oczekiwanie na klienta" << std::endl;
	try
	{
		_Akceptor.accept(_Socket);
		generujMapeDoWyslania();
		wyslijDane();
	}catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}

void SerwerTCP::wyslijDane()
{
	wyswietlMape(mapa);
	if(czyKoniecGry())
	{
		//wyswietlMape(mapa);
		mapa[9] = 'o';
		std::cout << "Koniec gry. Wygralo kolko" << std::endl;
		_Socket.send(asio::buffer(mapa));
		return;
	}else if(_zapelnione == 9)
	{

		mapa[9] = 'r';
		std::cout << "Koniec gry - remis" << std::endl;
		_Socket.send(asio::buffer(mapa));
		return;
	}
	while(true)
	{
		std::string pozycja;
		std::cout << "Wprowadz pozycje: ";
		std::cin >> pozycja;
		if(sprawdzPozycje(pozycja))
		{
			int x = pozycja[0] - 65;
			int y = pozycja[1] - 49;
			_Plansza[x][y] = KRZYZYK;
			++_zapelnione;
			mapa[x+3*y] = 'x';
			
			break;
		}else
		{
			std::cout << "Niepoprawna pozycja" << std::endl;
		}
	}
	wyswietlMape(mapa);
	if(czyKoniecGry())
	{
		//std::string koniec = "KN";
		mapa[9] = 'x';
		std::cout << "Koniec gry. Wygral krzyzyk" << std::endl;
		_Socket.send(asio::buffer(mapa));
		return;
	}else if(_zapelnione == 9)
	{
		mapa[9] = 'r';
		std::cout << "Koniec gry - remis" << std::endl;
		_Socket.send(asio::buffer(mapa));
		return;
	}
	wyswietlMape(mapa);
	//_Klient.send(asio::buffer(mapaWysylana));
	asio::write(_Socket, asio::buffer(mapa));
	odbierzDane();
}

void SerwerTCP::odbierzDane()
{
	std::cout << "Oczekiwanie na drugiego gracza" << std::endl;
	try{
		_Socket.read_some(asio::buffer(pozycjaOdbierana, 3));
	}catch(system::system_error& e)
	{
		std::cout << "Klient sie rozlaczyl" << std::endl;
		return;
	}
	pozycjaOdbierana[2] = '\0';
	int x = pozycjaOdbierana[0] - 65;
	int y = pozycjaOdbierana[1] - 49;
	mapa[x+3*y] = 'o';
	_Plansza[x][y] = KOLKO;
	++_zapelnione;

	wyslijDane();
}

void SerwerTCP::generujMapeDoWyslania()
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if(_Plansza[i][j] == PUSTE)
				mapa[3*i+j] = ' ';
			else if(_Plansza[i][j] == KRZYZYK)
				mapa[3*i+j] = 'x';
			else if(_Plansza[i][j] == KOLKO)
				mapa[3*i+j] = 'o';
		}
	}
	mapa[9] = '\0';
}

bool SerwerTCP::czyKoniecGry()
{
	// Pierwsza kolumna
	if(_Plansza[0][0] != PUSTE &&
	 	(_Plansza[0][0] == _Plansza[0][1]) && 
		(_Plansza[0][0] == _Plansza[0][2]))
		return true;
	//druga kolumna
	else if (_Plansza[1][0] != PUSTE &&
	 		(_Plansza[1][0] == _Plansza[1][1]) && 
	 		(_Plansza[1][0] == _Plansza[1][2]))
		return true;
	//trzecia kolumna
	else if (_Plansza[2][0] != PUSTE &&
	 		(_Plansza[2][0] == _Plansza[2][1]) && 
	 		(_Plansza[2][0] == _Plansza[2][2]))
		return true;
	//pierwszy wiersz
	else if (_Plansza[0][0] != PUSTE &&
	 		(_Plansza[0][0] == _Plansza[1][0]) && 
	 		(_Plansza[0][0] == _Plansza[2][0]))
		return true;
	//drugi wiersz
	else if (_Plansza[0][1] != PUSTE &&
	 		(_Plansza[0][1] == _Plansza[1][1]) && 
	 		(_Plansza[0][1] == _Plansza[2][1]))
		return true;
	//trzeci wiersz
	else if (_Plansza[0][2] != PUSTE &&
	 		(_Plansza[0][2] == _Plansza[1][2]) && 
	 		(_Plansza[0][2] == _Plansza[2][2]))
		return true;
	//pierwszy skos
	else if (_Plansza[0][0] != PUSTE &&
	 		(_Plansza[0][0] == _Plansza[1][1]) && 
	 		(_Plansza[0][0] == _Plansza[2][2]))
		return true;
	//drugi skos
	else if (_Plansza[0][2] != PUSTE &&
	 		(_Plansza[0][2] == _Plansza[1][1]) && 
	 		(_Plansza[0][2] == _Plansza[2][0]))
		return true;
	else
		return false;
}