#include <boost/asio.hpp>

#pragma once

class GraTCP
{
public:
	virtual void run() = 0;
	GraTCP();

protected:
	boost::asio::io_service _Serwis;
	boost::asio::ip::tcp::socket _Socket;
	char mapa[10];

	virtual void wyslijDane() = 0;
	virtual void odbierzDane() = 0;
	bool sprawdzPozycje(const std::string& pos);
	void wyswietlMape(const char* mapa);
};