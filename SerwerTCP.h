#include <boost/asio.hpp>
#include "GraTCP.h"

#pragma once

class SerwerTCP: public GraTCP
{
public:
	SerwerTCP();
	virtual void run();

private:
	boost::asio::ip::tcp::acceptor _Akceptor;
	enum StanPola {PUSTE, KRZYZYK, KOLKO};
	StanPola _Plansza[3][3];
	char pozycjaOdbierana[3];
	unsigned _zapelnione;

	virtual void wyslijDane();
	virtual void odbierzDane();
	void generujMapeDoWyslania();
	bool czyKoniecGry();
};