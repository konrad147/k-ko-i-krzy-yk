#include "KlientTCP.h"
#include <iostream>

using boost::asio::ip::tcp;
using namespace boost;

KlientTCP::KlientTCP(const char* adres): _Rozwiazywacz(_Serwis),
										_Zapytywacz(adres, "1500")
{}

void KlientTCP::run()
{
	try{
		asio::connect(_Socket, _Rozwiazywacz.resolve(_Zapytywacz));
	}catch(system::system_error& e)
	{
		std::cout << "Nie udalo sie nawiazac polaczenia" << std::endl;
		return;
	}
	odbierzDane();	
}

void KlientTCP::wyslijDane(){
	std::string pozycja;
	while(true)
	{
		std::cout << "Wprowadz pozycje: ";
		std::cin >> pozycja;

		if(sprawdzPozycje(pozycja))
		{
			try{
				asio::write(_Socket, asio::buffer(pozycja));
			}catch(system::system_error& e)
			{
				std::cout << "Utracono polaczenie z serwerem" << std::endl;
				return;
			}
			//_Socket.send(asio::buffer(pozycja));
			int x = pozycja[0] - 65;
			int y = pozycja[1] - 49;
			mapa[x+3*y] = 'o';
			break;
		}
		else{
			std::cout << "Niepoprawna pozycja" << std::endl;
		}
		//Socket.read_some(asio::buffer(mapa, 200));
	}
	wyswietlMape(mapa);
	odbierzDane();
}

void KlientTCP::odbierzDane()
{
	std::cout << "Oczekiwanie na drugiego gracza" << std::endl;
	try{
		_Socket.read_some(asio::buffer(mapa, 10));
	}catch(system::system_error& e)
	{
		std::cout << "Utracono polaczenie z serwerem" << std::endl;
		return;
	}
	
	wyswietlMape(mapa);

	if(mapa[9] == 'x')
	{

		std::cout << "Koniec Gry. Wygral krzyzyk" << std::endl;
		return;
	}else if(mapa[9] == 'o')
	{
		std::cout << "Koniec Gry. Wygralo kolko" << std::endl;
		return;
	}else if(mapa[9] == 'r')
	{
		std::cout << "Koniec Gry - remis" << std::endl;
		return;
	}

	wyslijDane();
}